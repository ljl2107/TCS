#pragma once
#include<stdio.h>
#include<graphics.h>
#include<conio.h>
#include<time.h>
#include<mmsystem.h>

#pragma comment(lib,"winmm.lib")//引用Windows Multimedia 
#pragma warning(disable:4996)
/*
开发日志：
1.首先完成基本功能的搭建。
2.实现变色功能。
3.计分版功能。
4.插入音乐。
*/
#define WIDTH 1200//UNIT*600
#define HIGH 660//UNIT*33
#define UNIT 20//这是每个蛇身矩形的单位长度
IMAGE img_bk;//设置背景图片
IMAGE img_frame, img_pause, img_manual, img_bkimg_rank;//边框图片,暂停图片,说明图片,排行榜背景
enum game//枚举设置空地，蛇身，蛇头，墙，食物所处的状态
{
	SPACE, BODY, HEAD, WALL, FOOD//0,1,2,3,4
};
struct SNAKE//蛇身的结构体
{
	int x;
	int y;
};
struct SNAKE snake[1024];//[0][0]储存蛇头的位置
int snakelong;//蛇身的长度
struct _FOOD//食物
{
	int color;
	int x;
	int y;
}food;
struct record//这是记录排行榜的数组
{
	int score = 0;//分数
	int hour;
	int min;
	int sec;
	int year;
	int mon;
	int day;
}rec[10];

/*————————————————定义全局变量————————*/
int h_num;//纵横的边框个数，只在绘制边框时有用
int w_num;
int canvas[HIGH][WIDTH - 120];//设置画布，用来存储当前的状态
int state;//状态，用来标识选项：选择的是 1 OR 2 OR 3 OR 4的选项状态
int snakeDir;//方向，用来标识蛇的移动方向
int snakecolor;//蛇的颜色
int score;//得分
int sleeptime;//调节速度用
int mode;//模式选择//普通，无敌
/*———————————————————————————————————*/
/*———————————————定义函数—————————————*/
void startup();//初始化函数
void chose();//选择函数
int setColor(int a);//设置填充颜色
void menu();//菜单函数
void pauseMenu();//暂停游戏菜单
void show();//展示函数
void updateWithoutuser();//与用户输入无关的更新
void updateWithuser();//与用户输入有关的更新
void gameover();//游戏结束
void regame();//重开游戏
void addFood();//随机设置食物
void speed_mode_menu();//速度，模式选择菜单。四种速度，三种模式
int writerank();
int showrank();
void autofind();//自动寻路
/*——————————————————————————————————————*/
