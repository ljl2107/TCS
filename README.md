# 贪吃蛇C++_2021

#### 介绍
大一作业，使用EasyX图形库制作的贪吃蛇游戏
 **具体可以参考我的这篇文章说明：** 
[我的CSDN博客，贪吃蛇](https://blog.csdn.net/m0_63288666/article/details/123450574?spm=1001.2014.3001.5502)

#### 软件说明
贪吃蛇

#### 安装教程

没什么好说的

#### 使用说明

正常贪吃蛇的游戏规则

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
